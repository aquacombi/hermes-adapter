<?php

namespace HermesCRM\Adapter\Adapters;

use HermesCRM\Adapter\Model\Invoice as InvoiceModel;

/**
 * Class Invoice
 * @package HermesCRM\Adapter\Adapters
 */
class Invoice extends Adapter
{
    /**
     * Path of products api
     * @var string
     */
    public $path = 'invoices';

    /**
     * @var array
     */
    protected $params = [
        'types'     => 'invoice',
        'present'   => 'type,date,due_date,balance,total_incl_vat,total_excl_vat,total_vat,status,payments,rows,shipping_status',
        'with'      => 'payments,rows'
    ];

    /**
     * Get all invoices from contact
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return collect($this->get());
    }

    /**
     * @param $where
     * @param $value
     *
     * @return $this
     */
    public function where($where, $value)
    {
        if (is_bool($value) && !is_numeric($value)) {
            // convert true/false to 'true'/'false'
            $value = $value ? 'true' : 'false';
        }

        $this->params[$where] = (string) $value;

        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function contact($id)
    {
        $this->where('contact_id', $id);

        return $this;
    }

    /**
     * @param string $id
     * @param array  $parameters
     *
     * @return \Illuminate\Support\Collection
     */
    public function get($id = '', $parameters = [])
    {
        $params = array_merge($this->params, $parameters);

        $result = parent::get($id, $params, [], []);

        return $result->getResult();
    }

    /**
     * @param        $id
     * @param string $disposition
     *
     * @return mixed
     */
    public function pdf($id, $disposition = 'attachment')
    {
        $result = parent::get($id .'/download', ['disposition' => $disposition]);

        return $result->getResult();
    }

    /**
     * Create a new invoice
     *
     * @param InvoiceModel $invoice
     */
    public function create(InvoiceModel $invoice)
    {
        return parent::post('', $invoice->getAttributes(), ['Accept' => 'application/json']);
    }

    /**
     * Pay invoice
     *
     * @param InvoiceModel $invoice
     * @param payment
     */
    public function pay(InvoiceModel $invoice, $payment)
    {
        return parent::post($invoice->id.'/pay', $payment, ['Accept' => 'application/json']);
    }
}