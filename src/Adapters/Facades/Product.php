<?php

namespace HermesCRM\Adapter\Adapters\Facades;

use Illuminate\Support\Facades\Facade;
use HermesCRM\Adapter\Adapters;

class Product extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Adapters\Product::class;
    }
}
