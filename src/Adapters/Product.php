<?php

namespace HermesCRM\Adapter\Adapters;

use Illuminate\Support\Facades\Cache;

/**
 * Class Product
 * @package HermesCRM\Adapter\Adapters
 */
class Product extends Adapter
{
    /**
     * Path of products api
     * @var string
     */
    public $path = 'products';

    /**
     * @var array
     */
    protected $params = ['onlyVisible' => true, 'with' => 'group,usps,specifications,shippingCosts', 'is_active' => 'true'];

    /**
     * @var string
     */
    protected $cachePrefix = 'hermes.products.';

    /**
     * @var string
     */
    protected $cacheConfig = 'hermes.caching.products';

    /**
     * Get all products
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return collect($this->get());
    }

    /**
     * @param $search
     *
     * @return $this
     */
    public function search($search)
    {
        $this->params['search'] = $search;

        return $this;
    }

    /**
     * Get all products in a group
     *
     * @param int $groupId
     *
     * @return mixed
     */
    public function inGroup($groupId)
    {
        if (! $this->result) {
            $this->get();
        }

        $collection = collect($this->result);

        return $collection->filter(function ($item) use ($groupId) {
            return $item->group_id == $groupId;
        });
    }

    /**
     * @param $where
     * @param $value
     *
     * @return $this
     */
    public function where($where, $value)
    {
        if (is_bool($value) && !is_numeric($value)) {
            // convert true/false to 'true'/'false'
            $value = $value ? 'true' : 'false';
        }

        $this->params[$where] = (string) $value;

        return $this;
    }

    /**
     * @param string $id
     * @param array  $parameters
     *
     * @return \Illuminate\Support\Collection
     */
    public function get($id = '', $parameters = [])
    {
        $params = array_merge($this->params, $parameters);

        $cacheKey = $this->cachePrefix . sha1($id . serialize($params));

        $this->result = Cache::remember($cacheKey, config($this->cacheConfig), function () use ($id, $params) {
            return parent::get($id, $params, [], [])->getResult();
        });

        return $this->result;
    }

    /**
     * Get product groups
     * @param array $filterIds filter by group ids
     * @return mixed
     */
    public function groups($filterIds = null)
    {
        if (! $this->result) {
            $this->get();
        }

        // get all the groups from product
        // and group by group_id
        $collection = collect($this->result)->pluck('group')->groupBy('id');

        $collection->each(function ($items, $id) use ($collection) {
            // remove empty groups
            if (! $items->first()) {
                return $collection->pull($id);
            }

            // group will be present for every
            // product in this group
            // grab the first one and use it as the group data
            $groupData = $items->first();

            // count the groups and use this as product count
            $groupData->product_count = count($items);

            // update the group with new data
            return $collection->put($id, $groupData);
        });

        if ($filterIds) {
            $collection = $collection->filter(function ($group) use ($filterIds) {
                return in_array($group->id, $filterIds);
            });
        }

        return $collection->values()->all();
    }
}