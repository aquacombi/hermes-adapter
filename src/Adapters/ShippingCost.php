<?php

namespace HermesCRM\Adapter\Adapters;

class ShippingCost extends Adapter
{
    /**
     * Path of contact api
     *
     * @var string
     */
    public $path = 'products';

    /**
     * Calculate shipping costs based on params
     * 
     * @param  array  $products [{'reference' => .., 'qty' => ..}]
     * @param  string $zipcode 
     * @param  string $country 
     * 
     * @return Collection
     */
    public function calculate(array $products, $zipcode = null, $country = 'NL')
    {
        $rows = parent::post('shipping', [
            'products'  => $products,
            'zipcode'   => $zipcode,
            'country'   => $country
        ], ['Accept' => 'application/json']);

        return $rows->getCollection();
    }
}
