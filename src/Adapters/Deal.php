<?php

namespace HermesCRM\Adapter\Adapters;

class Deal extends Adapter
{
    /**
     * Path of deal api
     * @var string
     */
    public $path = 'deals';
}