<?php

namespace HermesCRM\Adapter\Adapters;

use Illuminate\Support\Facades\Config;
use Appstract\LushHttp\Lush;
use InvalidArgumentException;

class Adapter
{

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $result;

    /**
     * @var array
     */
    protected $defaultHeaders = [
        'X-Powered-By' => 'Hermes'
    ];

    /**
     * @var array
     */
    public $requiredConfigValues = [
        'hermes.tenant.url',
        'hermes.tenant.username',
        'hermes.tenant.password',
    ];

    /**
     * Adapter constructor.
     */
    public function __construct()
    {
        $this->setConfig();
    }

    /**
     * @return Lush
     */
    public function getClient()
    {
        if (! $this->client) {
            $this->client = new Lush($this->getBaseUrl(), [
                'username' => $this->config['tenant']['username'],
                'password' => $this->config['tenant']['password'],
                'fail_on_error' => false
            ]);
        }

        return $this->client;
    }

    /**
     * @return mixed
     */
    protected function getBaseUrl()
    {
        return $this->config['tenant']['url'];
    }

    /**
     *
     */
    protected function setConfig()
    {
        $config = Config::get('hermes');

        $this->validateConfig($config);

        $this->config = $config;
    }

    /**
     * @param array $config
     */
    protected function validateConfig(array $config = [])
    {
        if (empty($config)) {
            throw new InvalidArgumentException('No config values found');
        }

        foreach ($this->requiredConfigValues as $key) {
            if (! Config::get($key)) {
                throw new InvalidArgumentException('Missing required value ['.$key.']');
            }
        }
    }

    /**
     * @param $method
     * @param $options
     *
     * @return mixed
     */
    protected function sendRequest($method, $options)
    {
        $url = isset($options[0]) ? $options[0] : '';

        if (!empty($this->path)) {
            $url = $this->path.'/'.$url;
        }

        $parameters = isset($options[1]) ? $options[1] : [];

        $headers = isset($options[2]) ? $options[2] : [];
        $headers = array_merge($headers, $this->defaultHeaders);

        $client = $this->getClient();

        $this->result = $client->$method($url, $parameters, $headers);
        return $this->result;
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return $this->sendRequest($method, $arguments);
    }
}