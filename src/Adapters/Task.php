<?php

namespace HermesCRM\Adapter\Adapters;

class Task extends Adapter
{
    /**
     * Path of Task api
     * @var string
     */
    public $path = 'tasks';
}