<?php

namespace HermesCRM\Adapter\Adapters;

class Mollie extends Adapter
{
    /**
     * Path of contact api
     *
     * @var string
     */
    public $path = 'mollie';

    /**
     * Fetches available issuers
     *
     * @return \Illuminate\Support\Collection
     */
    public function issuers()
    {
        return (parent::get('issuers', [], ['Accept' => 'application/json']))->getCollection();
    }

    /**
     * Fetches available methods
     *
     * @return \Illuminate\Support\Collection
     */
    public function methods()
    {
        return (parent::get('methods', [], ['Accept' => 'application/json']))->getCollection();
    }
}
