<?php

namespace HermesCRM\Adapter\Adapters;

use Appstract\LushHttp\Exception\LushRequestException;

class Auth extends Adapter
{
    /**
     * @var array
     */
    protected $params = [
        'sort' => 'updated_at',
        'order' => 'desc',
        'limit' => 1,
        'present' => 'contact,contact.full_name,contact.formal_salutation,contact.email_address,contact.email,contact.email.created_by,contact.phone_number',
        'with' => 'contact.address,contact.addresses,contact.emails,contact.phones,contact.creditEntries,contact.placements,contact.placements.latestContract,contact.placements.device,contact.placements.device.type',
    ];

    /**
     * @var array
     */
    protected $tokenParams = [
        'send_notification' => 'true',
    ];

    /**
     * Path of auth api
     * @var string
     */
    public $path = '';

    /**
     * @var string
     */
    public $authPath = 'auth/';

    /**
     * @var string
     */
    public $contactsPath = 'auth/contacts/';

    /**
     * @param string $id
     * @param array  $parameters
     *
     * @return \Illuminate\Support\Collection
     */
    public function get($id = '', $parameters = [])
    {
        $params = array_merge($this->params, $parameters);

        $result = parent::get($this->contactsPath . $id, $params, [], []);

        return $result->getResult();
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data)
    {
        array_set($data, 'target_website', url('/'));

        $result = parent::post($this->contactsPath, $data, ['Accept' => 'application/json']);

        return $result->getResult();
    }

    /**
     * Update contact data
     *
     * @param $contactId
     * @param $data
     *
     * @return mixed
     */
    public function update($contactId, $data)
    {
        $result = parent::put($this->contactsPath . $contactId, $data, ['Content-Type' => 'application/json']);

        return $result->getResult();
    }

    /**
     * Update by contact account url
     *
     * @param $account
     * @param $data
     *
     * @return mixed
     */
    public function updateByAccount($account, $data)
    {
        $result = parent::put($account->getAccountBaseUrl(), $data, ['Content-Type' => 'application/x-www-form-urlencoded', 'Accept' => 'application/json']);

        return $result->getResult();
    }


    /**
     * @param $data
     *
     * @return mixed
     */
    public function login($data)
    {
        $params = array_merge($this->params, $data);

        $result = parent::post($this->contactsPath . 'login', $params, [], []);

        return $result->getResult();
    }

    // token shizzle

    /**
     * validate password reset token (before reset form)
     * 
     * @param  string $token
     * 
     * @return mixed
     */
    public function validateResetToken($token)
    {
        try {
            $result = parent::get($this->contactsPath . 'validate_reset_token/' . $token);
        } catch (LushRequestException $e) {
            $result = $e->getResponse();
        }

        return $result->getResult();
    }

    /**
     * create new token
     *
     * @param $user
     * @param $token
     *
     * @return mixed
     */
    public function createToken($user, $token)
    {
        $params = array_merge($this->tokenParams, [
            'email'          => $user->getEmailForPasswordReset(),
            'token'          => $token,
            'target_website' => url('/')
        ]);

        return parent::post($user->getAccountBaseUrl() . '/tokens', $params);
    }

    /**
     * delete tokens from user
     *
     * @param $user
     *
     * @return mixed
     */
    public function deleteTokens($user)
    {
        return parent::delete($user->getAccountBaseUrl() . '/tokens', $this->tokenParams);
    }

    /**
     * delete expired tokens
     *
     * @param $user
     *
     * @return mixed
     */
    public function deleteExpiredTokens()
    {
        $params = array_merge($this->tokenParams, ['expired' => 1]);

        return parent::delete($this->authPath . 'tokens', $params);
    }

    /**
     * check if token exists
     *
     * @param $user
     * @param $token
     *
     * @return mixed
     */
    public function tokenExists($user, $token)
    {
        $params = array_merge($this->tokenParams, ['email' => $user->getEmailForPasswordReset(), 'token' => $token]);

        return parent::get($user->getAccountBaseUrl() . '/tokens', $params);
    }
}
