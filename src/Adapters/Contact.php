<?php

namespace HermesCRM\Adapter\Adapters;

use Illuminate\Support\Facades\Log;

class Contact extends Adapter
{
    /**
     * Path of contact api
     *
     * @var string
     */
    public $path = 'contacts';

    /**
     * @var array
     */
    protected $params = ['present' => 'true', 'with' => 'addresses', 'merges' => 'true'];

    /**
     * @var
     */
    protected $meta; // contact meta data

    /**
     * @param string $id
     * @param array  $parameters
     *
     * @return \Illuminate\Support\Collection
     */
    public function get($id = '', $parameters = [])
    {
        $params = array_merge($this->params, $parameters);

        $result = parent::get($id, $params, [], []);

        return $result->getResult();
    }

    /**
     * Retrieve meta data for said contact
     *
     * @return array $returnMeta - e.g. ['hardness_dh' => 12]
     */
    public function getMeta()
    {
        if ($this->meta) {
            return $this->meta;
        }

        // check for meta presence in contact data
        if (!$this->result) {
            Log::error('[Hermes] Contact request not made yet.');
            return [];
        } else if (is_null($this->result->meta)) {
            Log::error('[Hermes] Meta data not present in contact. Use: [with => meta]');
            return [];
        }

        // transform meta
        $responseMetas = $this->result->meta;

        foreach ($responseMetas as $responseMeta) {
            $responseMeta = (array)$responseMeta;
            $this->meta[array_get($responseMeta, 'key', null)] = $responseMeta;
        }

        return $this->meta;
    }

    /**
     * Get meta value by key
     *
     * @param        $key
     * @param string $default
     *
     * @return string
     */
    public function getMetaValue($key, $default = '')
    {
        $meta = $this->getMeta();

        return isset($meta[$key]) ? $meta[$key]['value'] : $default;
    }

    /**
     * Get invoices from contact
     *
     * @param     $id
     * @param int $limit
     * @param int $offset
     *
     * @return mixed
     */
    public function invoices($id, $limit = 100, $offset = 0)
    {
        $params = [
            'limit'     => $limit,
            'offset'    => $offset,
            'present'   => 'type,date,due_date,balance,total_incl_vat,total_excl_vat,total_vat,status,payments,rows',
            'with'      => 'payments,rows'
        ];

        $result = parent::get($id.'/invoices', $params, [], []);

        return $result->getResult();
    }

    /**
     * @param $contactId
     * @param $addressId
     *
     * @return mixed
     */
    public function deleteAddress($contactId, $addressId)
    {
        $result = parent::delete($contactId.'/addresses/'.$addressId);

        return $result->getResult();
    }

    /**
     * @param      $contactId
     * @param      $description
     * @param      $amount
     * @param null $invoiceId
     * @param null $referenceId
     *
     * @return mixed
     */
    public function addCredit($contactId, $description, $amount, $invoiceId = null, $referenceId = null)
    {
        $result = parent::post($contactId.'/credit', [
            'invoice_id' => $invoiceId,
            'contact_id' => $contactId,
            'reference_contact_id' => $referenceId,
            'description' => $description,
            'amount' => $amount,
            'validate_balance' => true
        ]);

        return $result->getResult();
    }
}