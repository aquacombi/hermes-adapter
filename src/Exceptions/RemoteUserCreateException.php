<?php

namespace HermesCRM\Adapter\Exceptions;

use Illuminate\Support\MessageBag;
use ErrorException;

class RemoteUserCreateException extends ErrorException
{
    /**
     * Error container
     * 
     * @var array
     */
    protected $errors = [];
    
    /**
     * Constructor
     * 
     * @param array $errors - ['key' => 'message']
     */
    public function __construct(array $errors = [])
    {
        parent::__construct('Could not register user');

        if (empty($errors)) {
            $this->errors['message'] = ['Could not register user'];
        } else {
            $this->errors = $errors;
        }
    }

    /**
     * Returns errors as MessageBag
     * 
     * @return MessageBag
     */
    public function getErrors()
    {
        return new MessageBag($this->errors);
    }
}
