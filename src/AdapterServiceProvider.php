<?php

namespace HermesCRM\Adapter;

use Illuminate\Support\Facades\Auth;
use HermesCRM\Adapter\Auth\RemoteUserProviderContract;
use Illuminate\Support\ServiceProvider;
use HermesCRM\Adapter\Auth\HermesUserProvider;

class AdapterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/hermes.php' => config_path('hermes.php'),
            ], 'config');
        }

        Auth::provider('hermes', function ($app, array $config) {
            return new HermesUserProvider();
        });

        $this->app->instance(RemoteUserProviderContract::class, $this->app['auth.driver']->getProvider());
    }
    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/hermes.php', 'hermes');
    }
}