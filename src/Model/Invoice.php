<?php

namespace HermesCRM\Adapter\Model;

use HermesCRM\Adapter\Adapters\Invoice as InvoiceAdapter;

/**
 * Class Product
 * @package HermesCRM\Adapter\Model
 */
class Invoice extends Model
{

    protected $appends = [
        'title',
        'pdf_file_name'
    ];

    /**
     * @var string
     */
    public static $adapterClass = InvoiceAdapter::class;

    /**
     * Get invoice by id
     *
     * @param $product
     *
     * @return mixed
     */
    public static function find($invoice)
    {
        $data = (array)(new static::$adapterClass)->get($invoice);

        return (new static($data));
    }

    /**
     * Get invoices from contact_id
     *
     * @param $product
     *
     * @return mixed
     */
    public static function contact($id)
    {
        $data = (array)(new static::$adapterClass)->contact($id)->all()->toArray();

        return (new static)->hydrate($data);
    }

    /**
     * Create a new invoice
     *
     * @return mixed
     */
    public function create()
    {
        return (new static::$adapterClass)->create($this);
    }

    /**
     * Pay invoice
     *
     * @return mixed
     */
    public function pay($payment)
    {
        return (new static::$adapterClass)->pay($this, $payment);
    }

    /**
     * @return mixed
     */
    public function pdf()
    {
        return (new static::$adapterClass)->pdf($this->id);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getTitleAttribute($value)
    {
        return $this->reference_formatted;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getPdfFileNameAttribute($value)
    {
        return 'invoice_' . $this->date . '_' . $this->title . '.pdf';
    }
}