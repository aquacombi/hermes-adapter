<?php

namespace HermesCRM\Adapter\Model;

use HermesCRM\Adapter\Adapters\Product as ProductAdapter;

/**
 * Class Product
 * @package HermesCRM\Adapter\Model
 */
class Product extends Model
{

    /**
     * @var string
     */
    public static $adapterClass = ProductAdapter::class;

    /**
     * Get product by id/slug
     *
     * @param $product
     *
     * @return mixed
     */
    public static function find($product)
    {
        $data = (array) (new static::$adapterClass)->get($product);

        return (new static($data));
    }

    /**
     * Get products in group
     *
     * @param $groupId
     *
     * @return mixed
     */
    public static function inGroup($groupId)
    {
        $data = (new static::$adapterClass)->inGroup($groupId)->toArray();

        return (new static)->hydrate($data);
    }

}