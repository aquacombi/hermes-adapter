<?php

namespace HermesCRM\Adapter\Model;

use HermesCRM\Adapter\Adapters\Product as ProductAdapter;

/**
 * Class Product
 * @package HermesCRM\Adapter\Model
 */
class Group extends Model
{

    /**
     * @var string
     */
    public static $adapterClass = ProductAdapter::class;

    /**
     * Get all entities
     *
     * @return mixed
     */
    public static function all()
    {
        $data = (new static::$adapterClass)->groups();

        return (new static)->hydrate($data);
    }

}