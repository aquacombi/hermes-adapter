<?php

namespace HermesCRM\Adapter\Model;

use Jenssegers\Model\Model as Modeler;

abstract class Model extends Modeler
{

    /**
     * Must be set by child
     *
     * @var null
     */
    public static $adapterClass = null;

    /**
     * Model constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->forceFill($data);
    }

    /**
     * Get all entities
     *
     * @return mixed
     */
    public static function all()
    {
        $data = (new static::$adapterClass)->all()->toArray();

        return (new static)->hydrate($data);
    }

    /**
     * Get entity by id/slug
     *
     * @param $id
     *
     * @return mixed
     */
    public static function find($id)
    {
        $collection = static::all();

        if (isset($collection[$id])) {
            return $collection[$id];
        }

        return [];
    }

    /**
     * take a few
     *
     * @param $count
     *
     * @return mixed
     */
    public static function take($count)
    {
        $data = (new static::$adapterClass)->all()->take($count)->toArray();

        return (new static)->hydrate($data);
    }
}