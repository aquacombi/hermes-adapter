<?php

namespace HermesCRM\Adapter\Jobs;

use HermesCRM\Adapter\Adapters\Contact;

class PushContact extends PushJob
{
    /**
     * Adapter
     */
    protected $adapterClass = Contact::class;

    /**
     * Config
     *
     * @var string
     */
    protected $requestDefaults = 'hermes.defaults.contact';

    /**
     * Find a contact by email
     * if we have exactly 1 match, use that contact's id
     *
     * @param null $email
     *
     * @return $this
     */
    public function findContactByEmail($email = null)
    {
        if (!$email) {
            $email = array_get($this->requestAttributes, 'email.address',
                array_get($this->requestAttributes, 'email')
            );
        }

        // Check if contact exists
        $contacts = (array) (new Contact())->get('', ['email' => $email]);

        // if we have exactly 1 match, append to this contact
        if (count($contacts) === 1 && isset($contacts[0]->id)) {
            $this->requestAttributes['id'] = $contacts[0]->id;
        }

        return $this;
    }

    /**
     * @param $addressId
     *
     * @return bool
     */
    public function deleteAddress($contactId = null, $addressId = null)
    {
        if (!$contactId && $this->requestAttributes['id']) {
            $contactId = $this->requestAttributes['id'];
        }

        return (new Contact())->deleteAddress($contactId, $addressId);
    }

    /**
     * @param      $description
     * @param      $amount
     * @param null $invoiceId
     * @param null $referenceId
     *
     * @return mixed
     */
    public function addCredit($description, $amount, $invoiceId = null, $referenceId = null)
    {
        $contactId = $this->requestAttributes['id'];

        return (new Contact())->addCredit($contactId, $description, $amount, $invoiceId, $referenceId);
    }
}
