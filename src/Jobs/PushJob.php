<?php

namespace HermesCRM\Adapter\Jobs;

use Log;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class PushJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * REST Adapter
     */
    protected $adapterClass = null;

    /**
     * Config
     *
     * @var string
     */
    protected $requestDefaults = null;

    /**
     * Original request attributes; populated on fillRequestAttributes
     *
     * @var array
     */
    protected $originalRequestAttributes = [];

    /**
     * Transformed and supplemented request attributes
     *
     * @var array
     */
    protected $requestAttributes = [];

    /**
     * Original response attributes; populated on response
     *
     * @var array
     */
    protected $originalResponseAttributes = [];

    /**
     * Transformed response attributes
     *
     * @var array
     */
    public $responseAttributes = [];

    /**
     * Instantiate new Command instance
     *
     * @param array $requestAttributes - Raw request attributes (e.g. form input)
     */
    public function __construct(array $requestAttributes = [])
    {
        $this->fillRequestAttributes($requestAttributes);
    }

    /**
     * Command handle
     *
     * @return array - Transformed response attributes
     */
    public function handle()
    {
        $this->push();

        return $this->responseAttributes;
    }

    /**
     * Append subentity to request payload
     *
     * @param  string $to      - Request key to append to; typically plural (e.g. `notes`, `phones`, `addresses`)
     * @param  array  $data
     *
     * @return $this           - Yes; multiple entities -> one request
     */
    public function appendEntity($to, array $data = [])
    {
        $bag = array_get($this->requestAttributes, $to, []);
        $bag = is_array($bag) ? $bag : [];

        $transformerMethod = 'transform' . Str::studly(str_replace('.', ' ', $to)) . 'Entity';

        // Check for transformer and "call it"
        if (method_exists($this, $transformerMethod)) {
            $data = $this->{$transformerMethod}($data);
        }

        // Supplement empty values with subentity defaults; if available
        if (($defaults = config('hermes.defaults.' . $to, false))) {
            foreach ($defaults as $key => $value) {
                $data[$key] = empty($data[$key]) ? $value : $data[$key];
            }
        }

        $bag[] = $data;

        array_set($this->requestAttributes, $to, $bag);

        return $this;
    }

    /**
     * Populates / transforms request attributes
     *
     * @param  array  $requestAttributes
     *
     * @return void
     */
    protected function fillRequestAttributes(array $requestAttributes = [])
    {
        $this->originalRequestAttributes = $requestAttributes;

        $this->requestAttributes = $this->transformRequestAttributes($requestAttributes);

        $this->supplementMissingWithRequestDefaults();
    }

    /**
     * Executes the request; transforms / populates response data
     *
     * @return void
     */
    protected function push()
    {
        $class = $this->adapterClass;

        if (! empty($this->requestAttributes['id'])) {
            Log::debug('[hermes] Execute PUT', ['attributes' => $this->requestAttributes, 'original' => $this->originalRequestAttributes]);
            $this->originalResponseAttributes = (new $class)->put($this->requestAttributes['id'], $this->requestAttributes);
        } else {
            Log::debug('[hermes] Execute POST', ['attributes' => $this->requestAttributes, 'original' => $this->originalRequestAttributes]);
            $this->originalResponseAttributes = (new $class)->post('', $this->requestAttributes);
        }

        $this->responseAttributes = $this->transformResponseAttributes($this->originalResponseAttributes);

        Log::debug('[hermes] Response received', [
            'attributes' => $this->responseAttributes,
            'original'   => $this->originalResponseAttributes
        ]);
    }

    /**
     * Transform returned response attributes; implement if needed
     *
     * @param  array $responseAttributes - Raw response attributes; as returned by adapter
     *
     * @return array $responseAttributes - Transformed response attributes
     */
    protected function transformResponseAttributes($responseAttributes)
    {
        return $responseAttributes;
    }

    /**
     * Transforms constructor-fed request attributes; implement if needed
     *
     * @param  array  $requestAttributes - Raw request attributes (e.g. form input)
     *
     * @return array $requestAttributes  - Transformed request attributes
     */
    protected function transformRequestAttributes(array $requestAttributes = [])
    {
        return $requestAttributes;
    }

    /**
     * Writes attribute defaults if no attribute is specified, provided:
     * - That the requestDefaults string points to a non-empty config array
     * - The attributes in the config array are not already present in the requestAttributes
     * - Happens after transforming / supplementing request data
     *
     * @return void
     */
    protected function supplementMissingWithRequestDefaults()
    {
        if (! $this->requestDefaults) {
            return;
        }

        $defaults = array_dot(config($this->requestDefaults, []));

        // Supplement missing values with their respective defaults
        foreach ($defaults as $key => $value) {
            if (empty(array_get($this->requestAttributes, $key))) {
                array_set($this->requestAttributes, $key, $value);
            }
        }

        // Cleanse any empty vars
        // Except for Trim Exceptions
        foreach (array_dot($this->requestAttributes) as $key => $value) {
            if (in_array($key, array_flip(config('hermes.trim_exceptions'))) && strlen(trim($value)) == 0) {
                array_set($this->requestAttributes, $key, '');
            } elseif (strlen(trim($value)) == 0) {
                array_forget($this->requestAttributes, $key);
            }
        }
    }
}