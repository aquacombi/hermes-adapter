<?php

namespace HermesCRM\Adapter\Auth\Passwords;

use Illuminate\Support\Str;
use Illuminate\Auth\Passwords\PasswordBrokerManager as Base;

class PasswordBrokerManager extends Base
{
    /**
     * Create a token repository instance based on the given configuration.
     *
     * @param  array $config
     *
     * @return \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    protected function createTokenRepository(array $config)
    {
        $key = $this->app['config']['app.key'];

        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        return new RemoteTokenRepository($key);
    }
}
