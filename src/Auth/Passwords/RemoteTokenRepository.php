<?php

namespace HermesCRM\Adapter\Auth\Passwords;

use Exception;
use HermesCRM\Adapter\Adapters\Facades\Auth as AuthAdapter;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;

use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Str;

class RemoteTokenRepository implements TokenRepositoryInterface
{

    /**
     * The hashing key used for generating tokens.
     *
     * @var string
     */
    protected $hashKey;

    /**
     * Create a new token repository instance.
     *
     * @param string          $hashKey
     */
    public function __construct($hashKey)
    {
        $this->hashKey = $hashKey;
    }

    /**
     * Create a new token record.
     * @implement
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return string
     */
    public function create(CanResetPasswordContract $user)
    {
        $this->deleteExisting($user);

        // We will create a new, random token for the user so that we can e-mail them
        // a safe link to the password reset form. Then we will insert a record in
        // the database so that we can verify the token within the actual reset.
        $token = $this->createNewToken();

        AuthAdapter::createToken($user, $token);

        return $token;
    }

    /**
     * Determine if a token record exists and is valid.
     * @implement
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $token
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $token)
    {
        try {
            $response = AuthAdapter::tokenExists($user, $token);

            if (! is_array($items = $response->getResult())) {
                $record = [];
            } else {
                $record = (array) head($items);
            }
        } catch (Exception $e) {
            $record = [];
        }

        return ! empty($foundToken = array_get($record, 'id', null));
    }

    /**
     * Determine if the given user recently created a password reset token.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return bool
     */
    public function recentlyCreatedToken(CanResetPasswordContract $user)
    {
        return false;
    }

    /**
     * Delete a token record by user.
     * @implement
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return void
     */
    public function delete(CanResetPasswordContract $user)
    {
        $this->deleteExisting($user);
    }

    /**
     * Delete expired tokens.
     * @implement
     *
     * @return void
     */
    public function deleteExpired()
    {
        try {
            AuthAdapter::deleteExpiredTokens();
        } catch (RequestException $e) {
        }
    }

    /**
     * Delete all existing reset tokens from the database.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return int
     */
    protected function deleteExisting(CanResetPasswordContract $user)
    {
        try {
            AuthAdapter::deleteTokens($user);
        } catch (Exception $e) {
        }
    }

    /**
     * Build the record payload for the table.
     *
     * @param  string  $email
     * @param  string  $token
     * @return array
     */
    protected function getPayload($email, $token = null)
    {
        $payload = [
            'email' => $email
        ];

        array_set($payload, 'target_website', config()->get('app.url'));

        if ($token !== null) {
            array_set($payload, 'token', $token);
        }

        return $payload;
    }

    /**
     * Create a new token for the user.
     *
     * @return string
     */
    public function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), $this->hashKey);
    }
}