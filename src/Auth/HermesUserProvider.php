<?php

namespace HermesCRM\Adapter\Auth;

use Appstract\LushHttp\Exception\LushRequestException;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use HermesCRM\Adapter\Exceptions\RemoteUserCreateException;
use HermesCRM\Adapter\Adapters\Facades\Auth as AuthAdapter;

class HermesUserProvider implements RemoteUserProviderContract
{
    /**
     * @param array $data
     *
     * @return HermesUser
     * @throws RemoteUserCreateException
     */
    public function create(array $data)
    {
        try {
            $user = AuthAdapter::create($data);

            return $this->getHermesUser($user);
        } catch (LushRequestException $e) {
            if (is_string($content = $e->getContent())) {
                $content = json_decode($content, true);
            }
            throw new RemoteUserCreateException((array) $content);
        }
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $user = null;

        try {
            $user = AuthAdapter::get($identifier);
        } catch (LushRequestException $e) {}

        return $this->getHermesUser($user);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = null;

        try {
            $user = AuthAdapter::get($identifier, ['remember_token' => $token]);
        } catch (LushRequestException $e) {}

        return $this->getHermesUser($user);
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        AuthAdapter::update($user->id, ['remember_token' => $token]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $user = null;

        if (! empty($credentials)) {
            try {
                $user = AuthAdapter::login($credentials);
            } catch (LushRequestException $e) { }
        }
        
        return $this->getHermesUser($user);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        if ( ! empty($user->id) && ! empty($user->contact)) {
            return true;
        }

        return false;
    }

    /**
     * @param $user
     *
     * @return HermesUser
     */
    protected function getHermesUser($user)
    {
        if (! is_null($user)) {
            return new HermesUser((array) $user);
        }
    }
}
