<?php

namespace HermesCRM\Adapter\Auth;

use Illuminate\Contracts\Auth\UserProvider;

interface RemoteUserProviderContract extends UserProvider
{
    /**
     * Create new Remote User
     * 
     * @param  array  $data
     * 
     * @return RemoteUser
     */
    public function create(array $data);
}
