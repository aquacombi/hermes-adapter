<?php

namespace HermesCRM\Adapter\Auth;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Auth\GenericUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword as ResetPasswordContract;
use HermesCRM\Adapter\Adapters\Facades\Auth as AuthAdapter;

class HermesUser extends GenericUser implements RemoteUserContract, ResetPasswordContract
{
    use CanResetPassword, Notifiable;

    /**
     * All of the user's attributes.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'login_hash',
        'remember_token'
    ];

    /**
     * Attributes that are public
     *
     * @var array
     */
    protected $publicAttributes = [
        'id',
        'contact_id',
        'name',
        'email',
        'contact',
        'last_login_at',
        'last_login_ip',
        'is_primary',
        'credit_balance',
        'hasInvoice',
        'hasShipping'
    ];

    /**
     * HermesUser constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Get the base URL for contact API calls
     *
     * @return string
     */
    public function getContactBaseUrl()
    {
        $contactId = array_get($this->attributes, 'contact_id', '');

        return 'contacts/' . $contactId;
    }

    /**
     * Get the base URL for account API calls
     *
     * @return string
     */
    public function getAccountBaseUrl()
    {
        $contactId = array_get($this->attributes, 'contact_id', null);

        return 'contacts/' . $contactId . '/accounts/' . $this->getAuthIdentifier();
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        $email = array_get($this->attributes, 'email');

        return $email;
    }

    /**
     * Get the e-mail address where password reset notification is sent.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->getEmailForPasswordReset();
    }

    /**
     * Password reset mail is handled by hermes
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token) {}

    /**
     * Fill the model with an array of attributes. Force mass assignment.
     *
     * @param  array  $attributes
     * @return $this
     */
    public function forceFill(array $attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->attributes[$attribute] = $value;
        }

        return $this;
    }

    /**
     * Only fill explicitly whitelisted attributes
     *
     * @param  array  $attributes
     * @return $this
     */
    public function fill(array $attributes)
    {
        if (empty($this->fillable)) return $this->forceFill($attributes);

        return $this->forceFill(array_only($attributes, $this->fillable));
    }

    /**
     * Get all fillable attributes
     *
     * @param  boolean $omitPass - Omit password attributes; if specified
     * @return array
     */
    public function getFillables($omitPass = false)
    {
        $attributes = $this->attributes;
        $fillables  = $this->fillable;

        if ($omitPass) {
            $fillables = array_diff($fillables, ['password', 'password_confirmation']);
        }

        return array_only($attributes, $fillables);
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return array
     */
    public function getPublicAttributes()
    {
        return array_only($this->attributes, $this->publicAttributes);
    }

    /**
     * Determines if this entity exists remotely (for HTTP method determination)
     *
     * @return bool
     */
    public function exists()
    {
        return ! empty(array_get($this->attributes, 'id', null));
    }

    /**
     * Save the user account data
     *
     * @param  boolean $omitPass - Omit password related attributes
     * @return $this - Altered account object
     */
    public function save($omitPass = false)
    {
        if ($this->exists()) {
            $this->attributes = (array) AuthAdapter::updateByAccount($this, $this->getFillables($omitPass));
        } else {
            $this->attributes = (array) AuthAdapter::create($this->getFillables($omitPass));
        }

        return $this;
    }

    /**
     * returns 'invoice' and 'shipping' address
     *
     * @return \Illuminate\Support\Collection
     */
    public function getOrderAddresses()
    {
        $addresses = collect($this->attributes['contact']->addresses);

        $labels = $addresses->map(function ($item) {
            return isset($item->label) ? $item->label : null;
        });

        // Billing address
        if ($labels->contains('_billing')) {
            $invoiceAddress = $addresses->where('label', '_billing')->first();
        } elseif ($labels->contains('_general')) {
            $invoiceAddress = $addresses->where('label', '_general')->first();
        } else {
            $invoiceAddress = $addresses->first();
        }

        // Shipping address
        if ($labels->contains('_webshop_delivery')) {
            $shippingAddress = $addresses->where('label', '_webshop_delivery')->first();
            $shipping = $this->resetAddress($shippingAddress, '_webshop_delivery');
        } elseif ($labels->contains('_delivery')) {
            $shippingAddress = $addresses->where('label', '_delivery')->first();
            $shipping = $this->resetAddress($shippingAddress, '_delivery');
        } else {
            $shippingAddress = [];
            $shipping = $this->resetAddress($shippingAddress, '_delivery');
        }

        return collect([
            'invoice' => $this->resetAddress($invoiceAddress, '_billing'),
            'shipping' => $shipping,
        ]);
    }

    /**
     * Returns transformed addres
     *
     * @param  Object $address
     * @param  String $label
     * @param  array  $unset
     *
     * @return \Illuminate\Support\Collection
     */
    public function resetAddress($address, $label, $unset = [])
    {
        $data = [
            'region',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by'
        ];

        $data = array_merge($unset, $data);

        $address = collect($address);
        $address->forget($data);
        $address->put('label', $label);

        return $address;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }
    }
}