<?php

namespace HermesCRM\Adapter\Auth;

use Illuminate\Contracts\Auth\Authenticatable;

interface RemoteUserContract extends Authenticatable
{
    /**
     * Get the base URL for contact API calls
     * 
     * @return string
     */
    public function getContactBaseUrl();

    /**
     * Get the base URL for account API calls
     * 
     * @return string
     */
    public function getAccountBaseUrl();
}
