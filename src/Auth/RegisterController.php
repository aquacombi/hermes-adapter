<?php

namespace HermesCRM\Adapter\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;
use HermesCRM\Adapter\Exceptions\RemoteUserCreateException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $user = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RemoteUserProviderContract $user)
    {
        $this->middleware('guest');

        $this->user = $user;
    }

    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        try {
            $remoteUser = $this->user->create($request->all());
        } catch (RemoteUserCreateException $e) {
            return redirect()->back()->withErrors($e->getErrors());
        }

        auth()->login($remoteUser);

        return redirect($this->redirectPath());
    }
}
