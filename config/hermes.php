<?php

return [

    'tenant' => [
        'url'       => env('HERMES_URL'),
        'username'  => env('HERMES_USER'),
        'password'  => env('HERMES_KEY')
    ],

    'caching' => [
        'products' => env('HERMES_PRODUCT_CACHE', 1), // in minutes
    ],

    'defaults' => [
        'contact' => [
            'gender' => 'family'
        ],

        'deal' => [
            'type' => [
                'name' => config('app.name')
            ],
            'campaign' => [
                'name'   => 'Default campaign',
                'source' => config('app.url'),
                'medium' => 'website'
            ]
        ],

        'notes' => [
            'subject'   => 'Note',
            'is_pinned' => 0,
            'action'    => '_note', // _mail, _call, _note
        ],

        'tasks' => [
            'name'        => 'New Task',
            'due_end'     => 'tomorrow',
            'category_id' => 12,
            'is_done'     => 0,
            'is_flagged'  => 0
        ]
    ],

    'trim_exceptions' => [
        'housenumber_addition',
    ]
];