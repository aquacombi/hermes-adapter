## Hermes CRM Adapters

```
use HermesCRM\Adapter\Adapters\Facades\Product;
...
Product::get(282); // product by id
Product::all(); // all products (max 100)
Product::search('something')->all(); // search product
Product::where('groups', 'Baskets')->all(); // all products where
Product::groups(); // all product groups
Product::groups([1,4,7]); // product groups by id
Product::inGroup(id); // all products in a given group id
```

```
use HermesCRM\Adapter\Adapters\Facades\Invoice;
...
Invoice::all(); // list all invoices
Invoice::get(16); // Invoice by id

Invoice::where('contact_id', 132)::all(); // list invoices from contact

--
PDF EXAMPLE:

$invoice = InvoiceModel::find(17);

    return response($invoice->pdf(), 200)
        ->header('Content-Type','application/pdf')
        ->header('Content-Disposition', 'attachment; filename="'. $invoice->pdfFileName.'"');
```

